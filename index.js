var express = require('express');
var app = express();
var request = require('request');
var zlib = require('zlib');
var fs = require('fs');
var { google } =  require("googleapis");
let youtube = google.youtube({
  version: "v3",
  auth: "AIzaSyBI35tGWD8opzDyu4OrD68-_s5avRtg1yU",
});



var options = {
   	url: '',
    referer: 'https://www.youtube.com',
    origin: 'https://www.youtube.com'
};

var googleVidRegex = new RegExp('.*googlevideo\.com','g');
var googleRedirectorUrl = 'https://redirector.googlevideo.com';

app.use('/vid/:vidURL', function(req, res) {
	var decodedURL = decodeURIComponent(req.params.vidURL);
	decodedURL = decodedURL.replace(/___per___/g,'%');
 	//console.log(decodedURL);
	// redirector is unnecessary for stream files
	if(decodedURL.substr(decodedURL.length - 5) != '.m3u8') {
	//	decodedURL = decodedURL.replace(googleVidRegex, googleRedirectorUrl);
	}
	
	options.url = decodedURL;
	req.pipe(request.get(options)).pipe(res)
		.on('error', function(e) {
			console.log('Pipe error in yt proxy:\n' + e);
		});
});

app.get('/', function(req, res){
    res.sendFile('index.html', { root: __dirname + "/public/" } );
});

app.get("/search", async function (req, res) {
  let q = req.query.searchText;
  let pageToken = req.params.pageToken;
  let youtubeResponse = await youtube.search.list({
    q,
    type: "video",
    part: "snippet",
    pageToken,
    maxResults: 25
  });
  res.send(youtubeResponse);
});




function writeCacheHeaders(req, res, path) {
	res.setHeader('Cache-Control', 'public, max-age: 2592000, must-revalidate');

	var stats = fs.statSync(path);
	var mtime = stats.mtime;
	mtime.setMilliseconds(0,0);

	var reqModDate = req.headers['if-modified-since'];

	res.setHeader('Last-Modified', mtime.toUTCString());

	//console.log("Requesting: " + path + " reqmod" + reqModDate + " mtime" + mtime);

	if(reqModDate != null && (new Date(reqModDate)).getTime() == mtime.getTime()) {
		//console.log("Cached!!");
		return true;
	}

	//console.log("not cached : (");
	return false;
}

app.use(function(req, res, next){
	try {
		var path = __dirname + '/public' + req.url;
		if(!fs.existsSync(path) || fs.lstatSync(path).isDirectory()) {
			return;
		}

		var raw = fs.createReadStream(path);
		var acceptEncoding = req.headers['accept-encoding'];
		if (!acceptEncoding) {
			acceptEncoding = '';
		}
		if (acceptEncoding.match(/\bgzip\b/)) {
			if(path.includes('Build') && !path.includes('.json') && ! path.includes('UnityLoader')) {
				if(writeCacheHeaders(req, res, path)) {
					res.writeHead(304, {});
					res.end();
				} else {
					 res.writeHead(200,{});
					//res.writeHead(200, { 'Content-Encoding': 'gzip' });
	    			raw.pipe(res);
	    		}
	    	} else {
	    		// cache asset bundles
	    		if(path.includes('AssetBundles/')) {
	    			if(writeCacheHeaders(req, res, path)) {
	    				res.writeHead(304, {});
						res.end();
	    				//console.log("asset bundle use cache");
		    		} else {
						res.writeHead(200);
	    				raw.pipe(res);
	    				//console.log("asset bundle new");
		    		}
	    		} else {
	    			res.writeHead(200, { 'Content-Encoding': 'gzip' });
	    			raw.pipe(zlib.createGzip()).pipe(res);
	    		}
	    	}
		} else {
			res.writeHead(200, {});
			raw.pipe(res).on('error', function(e) {
				console.log('Pipe error in file pipe:\n' + e);
			});
		}
	} catch(ex) {
		console.log(ex);
	}

	next();
});

app.set('port', (process.env.PORT || 3000));

app.listen(app.get('port'), function() {
  	console.log("Node app is running at localhost:" + app.get('port'));
});
