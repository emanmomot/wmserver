
function httpGetAsync(theUrl, callback)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

var url;
var urlReady;

function getURLCallback(res) {
	console.log(res);
	url = res.url;
	urlReady = true;
}

//httpGetAsync("/proxy/ow2zBxRRCVo");

function animate() {
	if(urlReady) {
		var movietex = LibraryWebGLMovieTexture.WebGLMovieTextureCreate("http://localhost:5000/vid");
		return;
	} else {
		httpGetAsync("/url", getURLCallback);
		window.requestAnimationFrame(animate);
	}
}

//animate();

var encodedURL = encodeURIComponent("https://r5---sn-5ualdn76.googlevideo.com/videoplayback?itag=135&id=o-AFmmDF1NO8tT5nmhGCIAs1APMScr0DFeliKrSkUege8B&lmt=1458184630199818&ei=_Y0cWfqpM4yAuAXZxKWgCQ&dur=217.300&key=yt6&ip=70.168.100.68&sparams=clen%2Cdur%2Cei%2Cgcr%2Cgir%2Cid%2Cinitcwndbps%2Cip%2Cipbits%2Citag%2Ckeepalive%2Clmt%2Cmime%2Cmm%2Cmn%2Cms%2Cmv%2Cpl%2Crequiressl%2Csource%2Cupn%2Cexpire&mime=video%2Fmp4&upn=RPrN28RuVgU&gcr=us&ipbits=0&pl=24&source=youtube&initcwndbps=1400000&gir=yes&expire=1495065181&mv=m&mt=1495043498&ms=au&clen=12346349&mn=sn-5ualdn76&mm=31&keepalive=yes&requiressl=yes&signature=CA15C7318B982E2FAF9EE869DF9EC5DA25DB89E0.7D84175D98EDDFE1AB56DAA3793AD525CE4EE85D&ratebypass=yes");
var movietex = LibraryWebGLMovieTexture.WebGLMovieTextureCreate("http://localhost:5000/vid/" + encodedURL);
